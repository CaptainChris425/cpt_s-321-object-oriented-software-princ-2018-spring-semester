﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


//Name: Christopher Young
//Id: 11579727



namespace ConsoleApp1
{
    public abstract class BINtree<T> where T : IComparable<T>
    {
        public class BinaryNode<T> where T : IComparable<T>
    {
        #region Init
        private T Data { get; set; }
        private BinaryNode<T> left = null;
        private BinaryNode<T> right = null;
        public BinaryNode(T value = default(T), BinaryNode<T> leftnode = null, BinaryNode<T> rightnode = null)
        {
        /*Sets data to default, left to null, right to null
         */
            Data = value;
            left = leftnode;
            right = rightnode;
        }
        #endregion
        #region Overrides For Node
        public override string ToString() { return Data.ToString(); }
        public static bool operator !=(BinaryNode<T> node1, BinaryNode<T> node2)
        {
            return !(node1 == node2);
        }
        public static bool operator ==(BinaryNode<T> node1, BinaryNode<T> node2)
        {
            if (node2 is null || node1 is null)
            {
                return (node1 is null);
            }
            if (node1.Data == null | node2.Data == null)
            { throw new ArgumentNullException("Cannot compare null values"); }
            return (node1.Data.Equals(node2.Data));
        }
        public override bool Equals(object value)
        {
            // Is null?
            if (value is null)
            {
                return false;
            }

            // Is the same object?
            if (Object.ReferenceEquals(this, value))
            {
                return true;
            }

            // Is the same type?
            if (value.GetType() != this.GetType())
            {
                return false;
            }

            return Equals(value);

        }
        public bool Equals(BinaryNode<T> node)
        {
            if (Data == null | node.Data == null)
            { throw new ArgumentNullException("Cannot compare null values"); }
            else { return (Data.Equals(node.Data)); }
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public static bool operator <(BinaryNode<T> node1, BinaryNode<T> node2)
        {
            if (node1 is null | node2 is null)
            {
                 throw new ArgumentNullException("Cannot compare null values");
            }
            if (node1.Data == null | node2.Data == null)
            { return false; }
            return (node1.Data.CompareTo(node2.Data) < 0);

        }
        public static bool operator >(BinaryNode<T> node1, BinaryNode<T> node2)
        {
            if (node1 is null | node2 is null)
            {
               throw new ArgumentNullException("Cannot compare null values");
            }
            if (node1.Data == null | node2.Data == null)
            { return false; }
            return (node1.Data.CompareTo(node2.Data) > 0);
        }
        #endregion
        #region Getters/Setters
        public void Setleft(BinaryNode<T> leftdata) { left = leftdata; }
        public void Setright(BinaryNode<T> rightdata) { right = rightdata; }
        public void Setdata(T datadata) { Data = datadata; }
        public bool Isempty() { return Data == null; }
        public bool Hasleft() { return (left != null); }
        public bool Hasright() { return (right != null); }
        public BinaryNode<T> Getleft() { return left; }
        public BinaryNode<T> Getright() { return right; }
        public T Getdata() { return Data; }
        #endregion
    }
        public abstract void Insert(T Data);
            //Inserts node of datatype T into tree with a repeatable position by some comparison 
        public abstract bool Contains(T Data);
            //True if item is in tree (search using insert algotithm)
        public abstract void Inordertraversal();
            //prints tree left,root,right
        public abstract void Preordertraversal();
            //prints tree root,left,right
        public abstract void Postordertraversal();
            //prints tree left,right,root
    }

    public class BSTtree<T> : BINtree<T> where T : IComparable<T>
    {
        bool rooted = false; //Determines if a tree has a root or not
        int count = 0; //Running total of nodes
        int height = 0; // height of tree
        BinaryNode<T> root; //null root node
        public BSTtree(){
            root = new BinaryNode<T>(); // null root node
        }
        private bool Contains(ref BinaryNode<T> curr, ref BinaryNode<T> val)
        {
            if (val < curr && curr.Getleft() != null) { BinaryNode<T> left = curr.Getleft(); return Contains(ref left, ref val); } //if less than go left
            else if (val > curr && curr.Getright() != null) { BinaryNode<T> right = curr.Getright(); return Contains(ref right, ref val); } // if greater than go right
            else if (val == curr) { return true; } //if found true
            else { return false; } // if not found false
        }
        
        private void Insert(ref BinaryNode<T> node, ref BinaryNode<T> curr)
        {
            if (!rooted) { root = node; count++; rooted = true; return; } //if not rooted, set root
            else
            {
                if (node < curr) //if insert node less than current 
                {
                    if (curr.Getleft() == null) { curr.Setleft(node); count++; return; } //if no 
                    else
                    {
                        BinaryNode<T> left = curr.Getleft();
                        Insert(ref node, ref left);
                    }
                }
                else if (node > curr)
                {
                    if (curr.Getright() == null) { curr.Setright(node); count++; return; }
                    else
                    {
                        BinaryNode<T> right = curr.Getright();
                        Insert(ref node, ref right);
                    }
                }
                else if (node == curr) { return; }
            }
        }
        private void Preordertraversal(ref BinaryNode<T> curr)
        {
            //root,left,right
            if (curr.Isempty() == true) { return; }
            else
            {
                Console.Write(curr + " ");
                if (curr.Getleft() != null)
                {

                    BinaryNode<T> left = curr.Getleft();
                    Preordertraversal(ref left);
                }
                
                if (curr.Getright() != null)
                {
                    BinaryNode<T> right = curr.Getright();
                    Preordertraversal(ref right);
                }
            }
            
        }
        private void Inordertraversal(ref BinaryNode<T> curr)
        {
            //left,root,right
            if (curr.Isempty() == true) { return; }
            else
            {
                if (curr.Getleft() != null)
                {
                    BinaryNode<T> left = curr.Getleft();
                    Inordertraversal(ref left);
                }
                Console.Write(curr + " ");
                if (curr.Getright() != null)
                {
                    BinaryNode<T> right = curr.Getright();
                    Inordertraversal(ref right);
                }
            }
        }
        private void Postordertraversal(ref BinaryNode<T> curr)
        {
            //left,right,root
            if (curr.Isempty() == true) { return; }
            else
            {
                if (curr.Getleft() != null)
                {
                    BinaryNode<T> left = curr.Getleft();
                    Postordertraversal(ref left);
                }
                
                if (curr.Getright() != null)
                {
                    
                    BinaryNode<T> right = curr.Getright();
                    Postordertraversal(ref right);
                }
                Console.Write(curr + " ");
            }
        }
        private void Height(ref BinaryNode<T> curr, int currheight = 0)
        {
            if (currheight > height) { height = currheight; }
            if (curr.Getleft() != null)
            {
                BinaryNode<T> left = curr.Getleft();
                Height(ref left, currheight + 1);
            }
            if (curr.Getright() != null)
            {
                BinaryNode<T> right = curr.Getright();
                Height(ref right, currheight + 1);
            }
            if (curr.Getright() == null && curr.Getleft() == null)
            {
                return;
            }
            return;

        }
        public override void Inordertraversal()
        {
            Inordertraversal(ref root);
        }
        override public bool Contains(T Data) 
        {
            BinaryNode<T> temp = new BinaryNode<T>(Data);
            return Contains(ref root, ref temp);
        }
        public override void Preordertraversal()
        {
            Preordertraversal(ref root);
        }

        public override void Postordertraversal()
        {
            Postordertraversal(ref root);
        }
        public override void Insert(T Data)
        {
            BinaryNode<T> temp = new BinaryNode<T>(Data);
            Insert(ref temp, ref root);
        }
        public int Height()
        {
            Height(ref root);
            height += 1;
            return height;
        }
        public int Count() { return count; }
        public double Minlevels()
        {
            bool cont = true;
            double i = 0;
            while (cont)
            {
                i++;
                if (Math.Pow(2.0, i) - 1 >= count) { cont = false; }
            }
            return i;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            BSTtree<int> mytree = new BSTtree<int>();
            string x = Console.ReadLine();
            string[] y = x.Split(separator: ' ');
            foreach (string q in y)
            {
                int data;
                Int32.TryParse(q, out data);
                mytree.Insert(data);
            }
            Console.WriteLine("in order");
            mytree.Inordertraversal();
            Console.WriteLine("\npost order");
            mytree.Postordertraversal();
            Console.WriteLine("\npreorder");
            mytree.Preordertraversal();
            Console.WriteLine("");
            int count = mytree.Count();
            Console.WriteLine("count: " + count);
            int height = mytree.Height();
            Console.WriteLine("height: " + height);
            double minlevels = mytree.Minlevels();
            Console.WriteLine(mytree.Contains(2));
            Console.WriteLine("Minimum levels for a tree with " + count + " nodes is :" + minlevels);
            Console.ReadLine();
            
        }
    }
}
