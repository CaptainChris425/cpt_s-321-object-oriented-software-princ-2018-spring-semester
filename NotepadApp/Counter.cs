﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Numerics;


namespace WindowsFormsApp1
{
    public partial class Counter : Form
    {
        public Counter()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Show basic description of program.
            textBox1.Text = "Open, Save, and Edit text files here...";
        }

        public void LoadText(TextReader textReader)
        {
            try
            {
                //read the textreader into string, including newline chars
                String line = textReader.ReadToEnd();
                //update textbox with textreader contents
                textBox1.Text = line;
            }
            catch (IOException) //Catch input/output errors
            {   
                Console.WriteLine("Failed to load file..");
                throw;
            }
        }

        public void SaveText(TextWriter textWriter)
        {
            try
            {
                //Write the textbox text to the textwriter file
                textWriter.Write(textBox1.Text);
            }
            catch (IOException) //Catch input/output errors
            {
                Console.WriteLine("Failed to save file..");
                throw;
            }
        }
        
        public void Displaycountinglist()
        {
            StringBuilder printer = new StringBuilder();
            printer.AppendLine("This program will show the count of unique values in");
            printer.AppendLine("a randomly sorted or unsorted list. Using three different algorithms");
            printer.AppendLine("Using a .NET dictionary to determine the number of unique numbers by:");
            printer.AppendLine("copying each element into a dictionary as keys ,keys are unique in a dictionary");
            printer.AppendLine("so no number was repeated. Then counted the number of keys in dictionary.");
            Countinglist rndlist = new Countinglist();
            rndlist.Addrandom(10000, 20000);
            int hashsetcount = rndlist.Hashsetcount();
            int o1count = rndlist.O1count();
            int sortedcount = rndlist.O1count();
            int easycount = rndlist.Easycount();
            printer.AppendLine("The hashmap count is :" + hashsetcount.ToString());
            printer.AppendLine("The O(1) count is : " + o1count.ToString());
            printer.AppendLine("The sorted count is : " + sortedcount.ToString());
            printer.AppendLine("The definite count is : " + easycount.ToString());
            textBox1.Text = printer.ToString();
        }

        public void Selection_openfile()
        {
            //Open File helper function
            Stream openstream;
            using (OpenFileDialog openFileDialog1 = new OpenFileDialog()) //Open an instance of openfiledialog
            {
                openFileDialog1.Filter = "txt files (*.txt)|*.txt|doc files (*.doc)|*.doc|All files (*.*)|*.*"; //Filter only .doc and .txt files
                openFileDialog1.FilterIndex = 1; //Default to txt file
                if (openFileDialog1.ShowDialog() == DialogResult.OK)    //If the openfiledialog was able to run and recieve input
                {
                    if ((openstream = openFileDialog1.OpenFile()) != null)  //set openstream to the file selected if it is not null
                    {
                        using (TextReader sr = new StreamReader(openstream)) //open an instance of a textreader using the file selected
                        {
                            LoadText(sr);   //call load text with passed in textreader
                        }

                    }

                }
            }
        }
        public void Selection_savefile()
        {
            //Save file helper function
            Stream savestream;
            using (SaveFileDialog saveFileDialog1 = new SaveFileDialog()) //Open an instance of savefiledialog
            {
                saveFileDialog1.Filter = "txt files (*.txt)|*.txt|doc files (*.doc)|*.doc|All files (*.*)|*.*"; //Filter only .doc and .txt files
                saveFileDialog1.FilterIndex = 1; //Default to txt file
                saveFileDialog1.RestoreDirectory = true;
                if (saveFileDialog1.ShowDialog() == DialogResult.OK) //If the openfiledialog was able to run and recieve input
                {
                    if ((savestream = saveFileDialog1.OpenFile()) != null) //set savestream to the file selected if it is not null
                    {
                        using (TextWriter sr = new StreamWriter(savestream)) //open an instance of a textreader using the file selected
                        {
                            SaveText(sr); //call save text with passed in textreader
                        }

                    }

                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Selection_openfile();
        }

        private void loadFibo50ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FibonacciTextReader fibreader = new FibonacciTextReader(50);
            LoadText(fibreader);
        }

        private void loadFibo100ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FibonacciTextReader fibreader = new FibonacciTextReader(100);
            LoadText(fibreader);
        }

        private void saveFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Selection_savefile();
        }
    }
    public class Countinglist : List<int>
    {
        public bool Addrandom(int n, int max)
        {
            try
            {
                Random rnd = new Random();
                for (int i = 0; i < n; i++) { this.Add(rnd.Next(0, max)); }
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public int Hashsetcount()
        {
            //Using a .NET dictionary to determine the number of unique numbers by:
            //copying each element into a dictionary as keys ,keys are unique in a dictionary
            //so no number was repeated. Then counted the number of keys in dictionary.
            try
            {
                if (this.Count() == 0) { return 0; }
                Dictionary<int, int> rnddict = new Dictionary<int, int>();
                foreach (int i in this) { if (!rnddict.ContainsKey(i)) { rnddict.Add(i, 0); } }
                //for every item in this list if it is NOT in a dictionary, then add it to the dict
                return rnddict.Count();
            }
            catch (Exception)
            {
                return 0;
                throw;
            }


        }

        public int O1count()
        {
            //Using O(1) storage and O(n) time to determine the number of unique numbers by:
            //enumerating numbers in a list and only counting them when it is tha last occurance
            //of the value from itself till the end. (add one at the end as last value is not checked
            //during algorithm and thus its value would not have been counted)
            try
            {
                if (this.Count() == 0) { return 0; }
                int o1count = 0;
                bool matchfound;
                for (int first = 0; first < this.Count() - 1; first++)
                {
                    matchfound = false;
                    for (int second = first + 1; second <= this.Count() - 1; second++)
                    {
                        if (!matchfound) { if (this[first] == this[second]) { matchfound = true; second = this.Count(); } }
                        //if a match was not found compare the items at each index if they match then exit the loop and dont count
                    }
                    if (!matchfound) { o1count++; }
                }
                return ++o1count;
            }
            catch (Exception)
            {
                return 0;
                throw;
            }

        }

        public int Sortedcount()
        {
            //Sorts the list and then enumerates values only adding to the unique count if
            //the next value is NOT the same as the current value
            try
            {
                if (this.Count() == 0) { return 0; }
                int sortedcount = 0;
                this.Sort();
                for (int i = 0; i < this.Count() - 1; i++)
                {
                    if (this[i] != this[i + 1]) { sortedcount++; }
                    //if the i index item is NOT equal the i+1th index item then 
                }
                return ++sortedcount;
            }
            catch (Exception)
            {
                return 0;
                throw;
            }
        }

        public int Easycount()
        {
            // a list natively can count distinct items
            return this.Distinct().Count();
        }
    }
    public class FibonacciTextReader : TextReader
    {
        //
        //Overrrides TextReader to calculate fibinacci numbers when readline is called
        //readtoend itterates readline n times
        //index stores current fib index
        //

        int n; int index = 1;
        string returnstring;
        BigInteger first = 0; BigInteger second = 1; BigInteger temp;
        
        public FibonacciTextReader(int n)
        {
            //Initiate with an n for max index
            this.n = n;
        }

        public void Reset()
        {
            this.index = 0;
        }

        override public string ReadLine()
        {
            if (index <= n)
            {
                //"index : fibnumber"
                this.returnstring = this.index.ToString() + " : " +this.first.ToString();
                //Fibonacci algorithm 
                this.temp = this.second;
                this.second += this.first;
                this.first = this.temp;
                this.index++;
                return this.returnstring; // return one line
            }
            else { return null; }
        }
        public override string ReadToEnd()
        {
            StringBuilder fullstring = new StringBuilder(); 
            while (this.index <= this.n) //get line for every index up to n
            {
                fullstring.AppendLine(this.ReadLine());
            }
            return fullstring.ToString();
        }

    }

}

