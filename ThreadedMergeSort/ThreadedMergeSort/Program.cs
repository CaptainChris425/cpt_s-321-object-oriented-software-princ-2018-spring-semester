﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;



namespace ThreadedMergeSort
{
    class Program
    {
        static void Main(string[] args)
        {
            Random numbers = new Random();
            var stopwatch = System.Diagnostics.Stopwatch.StartNew();
            int[] n = new int[] { 8, 64, 256, 1024 };
            List<TimeSpan> TimesNonThreaded = new List<TimeSpan> { };
            List<TimeSpan> TimesThreaded = new List<TimeSpan> { };
            for (int i=0;i< n.Count(); i++)
            {
                List<int> arrT = new List<int> { };
                List<int> arr = new List<int> { };
                for (int j = 0; j < n[i]; j++)
                {
                    arr.Add(numbers.Next(0, Int32.MaxValue));
                    arrT.Add(numbers.Next(0, Int32.MaxValue));
                }
                stopwatch = System.Diagnostics.Stopwatch.StartNew();
                Mergesort(arr, 0, arr.Count - 1);
                stopwatch.Stop();
                TimesNonThreaded.Add(stopwatch.Elapsed);
                stopwatch = System.Diagnostics.Stopwatch.StartNew();
                ThreadedMergesort(arrT, 0, arr.Count - 1);
                stopwatch.Stop();
                TimesThreaded.Add(stopwatch.Elapsed);
            }

            Console.WriteLine("Times for non threaded mearge sort at different N values of random numbers");
            Console.WriteLine("===============================================================");
            for (int i = 0; i< TimesNonThreaded.Count(); i++)
            {
                Console.Write(" || n= " + n[i] + " : ");
                Console.Write("Time = " + TimesNonThreaded[i]);
            }
            Console.WriteLine("\n===============================================================\n\n");
            Console.WriteLine("Times for threaded mearge sort at different N values of random numbers");
            Console.WriteLine("===============================================================");
            for (int i = 0; i < TimesThreaded.Count(); i++)
            {
                Console.Write("|| n= " + n[i] + " : ");
                Console.Write("Time = " + TimesThreaded[i]);
            }
            Console.ReadLine();

        }

        static void Mergesorthelper(List<int> arr, int l, int m, int r)
        {
            
            int n1 = m - l +1;
            int n2 = r - m;
            int[] arrL = new int[n1];
            int[] arrR = new int[n2];

            for(int x = 0; x < n1; x++)
            {
                arrL[x] = arr[x+l];
            }
            for(int x = 0; x < n2; x++)
            {
                arrR[x] = arr[x + +m+1];
            }

            int i = 0;int j = 0;int k = l;
            while(i<n1 && j < n2)
            {
                if (arrL[i] <= arrR[j])
                {
                    arr[k] = arrL[i];
                    i++;
                }
                else
                {
                    arr[k] = arrR[j];
                    j++;
                }
                k++;
            }
            while (i < n1)
            {
                arr[k] = arrL[i];
                i++;
                k++;
            }
            while (j < n2)
            {
                arr[k] = arrR[j];
                j++;
                k++;
            }


        }
        

        class Mergearray
        {
            public int[] arr
            {
                get;
                set;
            }
            public int l
            {
                get;
                set;
            }
            public int m
            {
                get;
                set;
            }
            public int r
            {
                get;
                set;
            }
        }

        static void Mergesort(List<int> arr,int l, int r)
        {
            
            if (l < r)
            {
                int m = l + (r - l) / 2;
                Mergesort(arr, l, m);
                Mergesort(arr, m + 1, r);
                Mergesorthelper(arr, l, m, r);
            }
        }
        static void ThreadedMergesort(List<int> arr, int l, int r)
        {

            if (l < r)
            {
                int m = l + (r - l) / 2;
                new Thread(() => ThreadedMergesort(arr, l, m)).Start();
                new Thread(() => ThreadedMergesort(arr, m + 1, r)).Start();
                new Thread(() => ThreadedMergesorthelper(arr, l, m, r)).Start();
            }
        }
        static void ThreadedMergesorthelper(List<int> arr, int l, int m, int r)
        {
            int n1 = m - l + 1;
            int n2 = r - m;
            int[] arrL = new int[n1];
            int[] arrR = new int[n2];

            for (int x = 0; x < n1; x++)
            {
                arrL[x] = arr[x + l];
            }
            for (int x = 0; x < n2; x++)
            {
                arrR[x] = arr[x + +m + 1];
            }

            int i = 0; int j = 0; int k = l;
            while (i < n1 && j < n2)
            {
                if (arrL[i] <= arrR[j])
                {
                    arr[k] = arrL[i];
                    i++;
                }
                else
                {
                    arr[k] = arrR[j];
                    j++;
                }
                k++;
            }
            while (i < n1)
            {
                arr[k] = arrL[i];
                i++;
                k++;
            }
            while (j < n2)
            {
                arr[k] = arrR[j];
                j++;
                k++;
            }


        }
        //static void ThreadedMergeSort(object mergearray)
        //{
        //    Mergearray ma = mergearray as Mergearray;
        //    if (ma.l < ma.r)
        //    {
        //        ma.m = ma.l + (ma.r - ma.l) / 2;
        //        int r = ma.r;
        //        ma.r = ma.m;
        //        Thread t1 = new Thread(ThreadedMergeSort);
        //        t1.Priority = ThreadPriority.Highest;
        //        t1.Start(ma);
        //        ma.r = r;
        //        ma.l = ma.m + 1;
        //        Thread t2 = new Thread(ThreadedMergeSort);
        //        t2.Priority = ThreadPriority.Highest;
        //        t2.Start(ma);
        //        Thread t3 = new Thread(ThreadedMergesorthelper);
        //        t3.Priority = ThreadPriority.Highest;
        //        t3.Start(ma);
        //    }
        //}


        //static void ThreadedMergesorthelper(object mergearray)
        //{
        //    Mergearray ma = mergearray as Mergearray;
        //    int l = ma.l;
        //    int r = ma.r;
        //    int m = ma.m;
        //    int n1 = m - l + 1;
        //    int n2 = r - m;
        //    int[] arrL = new int[n1];
        //    int[] arrR = new int[n2];

        //    for (int x = 0; x < n1; x++)
        //    {
        //        arrL[x] = ma.arr[x + l];
        //    }
        //    for (int x = 0; x < n2; x++)
        //    {
        //        arrR[x] = ma.arr[x + +m + 1];
        //    }

        //    int i = 0; int j = 0; int k = l;
        //    while (i < n1 && j < n2)
        //    {
        //        if (arrL[i] <= arrR[j])
        //        {
        //            ma.arr[k] = arrL[i];
        //            i++;
        //        }
        //        else
        //        {
        //            ma.arr[k] = arrR[j];
        //            j++;
        //        }
        //        k++;
        //    }
        //    while (i < n1)
        //    {
        //        ma.arr[k] = arrL[i];
        //        i++;
        //        k++;
        //    }
        //    while (j < n2)
        //    {
        //        ma.arr[k] = arrR[j];
        //        j++;
        //        k++;
        //    }


        //}
    }
}
